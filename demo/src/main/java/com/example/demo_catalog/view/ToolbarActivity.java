package com.example.demo_catalog.view;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.support.annotation.LayoutRes;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.demo_catalog.R;

/**
 * Created by chochito on 21/01/16.
 */
public class ToolbarActivity extends AppCompatActivity {
    //https://www.behance.net/search?content=projects&user_tags=977387
    private ProgressDialog mProgressDialog;

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        View view = getLayoutInflater().inflate(R.layout.layout_toolbar, null);
        FrameLayout activityContainer = (FrameLayout) view.findViewById(R.id.activity_content);
        getLayoutInflater().inflate(layoutResID, activityContainer, true);
        super.setContentView(view);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) findViewById(R.id.toolbar_title);

        try {
            ActivityInfo activityInfo = getPackageManager().getActivityInfo(getComponentName(), PackageManager.GET_META_DATA);
            String activityTitle = activityInfo.loadLabel(getPackageManager()).toString();
            title.setText(activityTitle);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

//            Drawable upArrow = ContextCompat.getDrawable(this, R.drawable.ic_action_home);
//            upArrow.setColorFilter(ContextCompat.getColor(this, R.color.primary_color),
//                    PorterDuff.Mode.SRC_ATOP);
//            getSupportActionBar().setHomeAsUpIndicator(upArrow);
        }
    }

    public void showProgress(boolean isCancelable, String message) {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(message);
            mProgressDialog.setCancelable(false);
            if (isCancelable) {
                String cancel = getResources().getString(android.R.string.cancel);
                mProgressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, cancel,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
            }
            mProgressDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    mProgressDialog = null;
                }
            });
            mProgressDialog.show();
        }
    }

    public void showProgressDefault() {
        showProgress(false, "");
    }

    public void dismissProgress() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
