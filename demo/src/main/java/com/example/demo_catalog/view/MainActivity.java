package com.example.demo_catalog.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.demo_catalog.R;
import com.example.demo_catalog.view.product.CatalogActivity;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends ToolbarActivity {

    private static final int TIMER = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                Intent intent = new Intent(MainActivity.this, CatalogActivity.class);
                startActivity(intent);
                finish();
            }
        };

        Timer timer = new Timer();
        timer.schedule(task, TIMER);
    }
}
