package com.example.demo_catalog.view.util;

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.example.demo_catalog.R;

/**
 * Created by chochito on 25/01/16.
 */
public class MessageHelper {

    public static final void showErrorMessage(Context context, String message){
        Snackbar snackbar = Snackbar.make(((Activity) context).findViewById(android.R.id.content),
                message, Snackbar.LENGTH_SHORT);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(context, android.R.color.holo_red_light));
        snackbar.show();
    }

    public static final void showOkMessage(Context context, String message){
        Snackbar snackbar = Snackbar.make(((Activity) context).findViewById(android.R.id.content),
                message, Snackbar.LENGTH_SHORT);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
        snackbar.show();
    }

}
