package com.example.demo_catalog.view.product;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.demo_catalog.R;
import com.example.demo_catalog.model.Product;
import com.example.demo_catalog.view.util.AbstractRecyclerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chochito on 18/10/16.
 */

public class CatalogAdapter extends AbstractRecyclerAdapter<Product, CatalogAdapter.ViewHolder> implements Filterable{

    private Context context;
    private OnProductSelectedListener listener;

    public CatalogAdapter(Context context, List<Product> products, OnProductSelectedListener listener){
        super(products);
        this.context = context;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.catalog_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Product product = getItem(position);
        String textPrice = context.getString(R.string.price_text);
        holder.txtPrice.setText(String.format(textPrice, product.getPrice()));
        holder.txtName.setText(product.getName());
        Glide.with(context).load(product.getUrlImage()).placeholder(R.drawable.placeholder).into(holder.productImage);
    }

    @Override
    public Filter getFilter() {
        return new CatalogFilter();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private final ImageView productImage;
        private final TextView txtName;
        private final TextView txtPrice;

        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Product product = mItems.get(getAdapterPosition());
                    listener.onProductSelected(product);
                }
            });
            productImage = (ImageView)itemView.findViewById(R.id.product_image);
            txtName = (TextView)itemView.findViewById(R.id.product_name);
            txtPrice = (TextView)itemView.findViewById(R.id.product_price);
        }
    }

    private class CatalogFilter extends Filter{

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            String text = constraint.toString().toLowerCase();
            FilterResults filterResults = new FilterResults();
            List<Product>temp = new ArrayList();
            if(text.isEmpty()){
                filterResults.values = mItemsAll;
                filterResults.count = mItemsAll.size();
            }else{
                for(Product product: mItemsAll){
                    if(product.getName().toLowerCase().contains(text)){
                        temp.add(product);
                    }
                }
                filterResults.values = temp;
                filterResults.count = temp.size();
            }
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mItems = (List<Product>) results.values;
            notifyDataSetChanged();
        }
    }

    public interface OnProductSelectedListener{
        void onProductSelected(Product product);
    }

}
