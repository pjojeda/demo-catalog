package com.example.demo_catalog.view.util;

import android.support.v7.widget.RecyclerView;

import java.util.List;

/**
 * Created by chochito on 09/10/15.
 */
public abstract class AbstractRecyclerAdapter<T, VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH>{

    protected List<T> mItems;
    protected List<T> mItemsAll;

    public AbstractRecyclerAdapter(List<T> items){
        this.mItems = items;
        mItemsAll = items;
    }

    public AbstractRecyclerAdapter(){

    }

    public void add(T entity){
        mItems.add(entity);
        mItemsAll.add(entity);
        notifyDataSetChanged();
    }

    public void add(List<T> items){
        mItems.addAll(items);
        mItemsAll.addAll(items);
        notifyDataSetChanged();
    }

    public void setItems(List<T> items){
        this.mItems = items;
        this.mItemsAll = items;
        notifyDataSetChanged();
    }

    public void remove(T entity){
        mItems.remove(entity);
        mItemsAll.remove(entity);
        notifyDataSetChanged();
    }

    public void remove(int position){
        mItems.remove(position);
        mItemsAll.remove(position);
        notifyDataSetChanged();
    }

    public T getItem(int position){
        return mItems.get(position);
    }

    public void clear(){
        mItems.clear();
        mItemsAll.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

}
