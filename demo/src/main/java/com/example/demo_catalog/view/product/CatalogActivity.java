package com.example.demo_catalog.view.product;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.demo_catalog.R;
import com.example.demo_catalog.manager.ProductManager;
import com.example.demo_catalog.manager.listener.OnSearchListener;
import com.example.demo_catalog.model.Product;
import com.example.demo_catalog.view.ToolbarActivity;
import com.example.demo_catalog.view.util.MessageHelper;
import com.example.demo_catalog.view.util.decoration.DividerItemDecoration;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.util.List;

/**
 * Created by chochito on 18/10/16.
 */

public class CatalogActivity extends ToolbarActivity implements CatalogAdapter.OnProductSelectedListener{

    private ProductManager productManager;
    private RecyclerView list;
    private ProgressBar progressBar;
    private TextView txtEmpty;
    private SwipyRefreshLayout swipyRefreshLayout;
    private CatalogAdapter adapter;
    private int page;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initVars();
        initViews();
        loadData();
    }

    private void initVars(){
        productManager = new ProductManager(CatalogActivity.this);
        page = 0;
    }

    private void initViews(){
        setContentView(R.layout.catalog_activity);
        swipyRefreshLayout = (SwipyRefreshLayout)findViewById(R.id.swipyrefreshlayout);
        swipyRefreshLayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                page++;
                loadData();
            }
        });
        progressBar = (ProgressBar)findViewById(R.id.progress);
        txtEmpty = (TextView)findViewById(R.id.txt_empty);
        list = (RecyclerView) findViewById(R.id.list);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(CatalogActivity.this);
        list.setLayoutManager(mLayoutManager);
        list.setHasFixedSize(true);
        list.addItemDecoration(new DividerItemDecoration(CatalogActivity.this));
    }

    private void loadData(){
        swipyRefreshLayout.setRefreshing(true);
        progressBar.setVisibility(View.VISIBLE);
        productManager.findAll(page, new OnSearchListener<Product>() {
            @Override
            public void onSearchCompleted(List<Product> products) {
                if(adapter == null){
                    adapter = new CatalogAdapter(CatalogActivity.this, products, CatalogActivity.this);
                    list.setAdapter(adapter);
                }else{
                    adapter.add(products);
                }
                if(adapter.getItemCount() == 0){
                    txtEmpty.setVisibility(View.VISIBLE);
                }else{
                    txtEmpty.setVisibility(View.GONE);
                }
                progressBar.setVisibility(View.GONE);
                swipyRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onError(String error) {
                progressBar.setVisibility(View.GONE);
                MessageHelper.showErrorMessage(CatalogActivity.this, error);
                swipyRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_catalog, menu);
        final MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return true;
            }
        });
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                return false;
            }
        });
        searchView.setSubmitButtonEnabled(false);
        searchView.setIconifiedByDefault(true);
        return true;
    }

    @Override
    public void onProductSelected(Product product) {
        Intent intent = new Intent(CatalogActivity.this, ProductDetailActivity.class);
        intent.putExtra(ProductDetailActivity.PARAM_PRODUCT, product);
        startActivity(intent);
    }
}
