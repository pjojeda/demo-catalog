package com.example.demo_catalog.view.product;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.demo_catalog.R;
import com.example.demo_catalog.model.Product;
import com.example.demo_catalog.view.ToolbarActivity;

/**
 * Created by chochito on 18/10/16.
 */

public class ProductDetailActivity extends ToolbarActivity {

    public static final String PARAM_PRODUCT = "productDetail.product";
    private Product product;
    private TextView txtName;
    private TextView txtDescription;
    private TextView txtPrice;
    private ImageView imageProduct;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initVars();
        initViews();
        loadData();
    }

    private void initVars(){
        product = (Product) getIntent().getSerializableExtra(PARAM_PRODUCT);
    }

    private void initViews(){
        setContentView(R.layout.product_detail_activity);
        txtName = (TextView)findViewById(R.id.product_name);
        txtDescription = (TextView)findViewById(R.id.product_description);
        txtPrice = (TextView)findViewById(R.id.product_price);
        imageProduct = (ImageView)findViewById(R.id.product_image);
    }

    private void loadData(){
        txtName.setText(product.getName());
        txtDescription.setText(product.getDescription());
        String textPrice = getString(R.string.price_text);
        txtPrice.setText(String.format(textPrice, product.getPrice()));
        Glide.with(ProductDetailActivity.this).load(product.getUrlImage()).placeholder(R.drawable.placeholder).into(imageProduct);
    }

}
