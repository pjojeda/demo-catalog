package com.example.demo_catalog.model;

import java.io.Serializable;

/**
 * Created by chochito on 18/10/16.
 */

public class Product extends BaseEntity<Long> implements Comparable<Product>, Serializable{

    private String name;
    private String description;
    private double price;
    private String urlImage;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    @Override
    public int compareTo(Product another) {
        return this.name.compareTo(another.getName());
    }
}
