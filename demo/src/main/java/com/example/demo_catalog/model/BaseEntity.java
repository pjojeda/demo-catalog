package com.example.demo_catalog.model;

import java.io.Serializable;

/**
 * Created by chochito on 29/10/15.
 */
public class BaseEntity<PK extends Serializable> implements Serializable {

    private PK id;

    public BaseEntity(){}

    public BaseEntity(PK id){
        this.id = id;
    }

    public PK getId() {
        return id;
    }

    public void setId(PK id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BaseEntity<?> that = (BaseEntity<?>) o;

        return !(id != null ? !id.equals(that.id) : that.id != null);

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
