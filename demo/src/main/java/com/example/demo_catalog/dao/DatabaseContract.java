package com.example.demo_catalog.dao;

/**
 * Created by Jose on 30/07/2016.
 */
public class DatabaseContract {

    interface Products{
        String ID = "id";
        String NAME = "name";
        String DESCRIPTION = "description";
        String PRICE = "price";
        String IMAGE_URL = "image_url";

    }
}
