package com.example.demo_catalog.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.os.Environment;
import android.provider.BaseColumns;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

/**
 * Created by Jose on 30/07/2016.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String TAG = DatabaseHelper.class.getSimpleName();

    private static final String DATABASE_NAME = "demo.db";
    private static DatabaseHelper sInstance;

    private static final int DATABASE_VERSION = 1;

    private final Context mContext;

    public interface Tables {
        String PRODUCTS = "products";
    }

    private DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mContext = context;
    }

    private static final String CREATE_PRODUCTS_TABLE = "CREATE TABLE "
            + Tables.PRODUCTS + " (" + DatabaseContract.Products.ID + " INTEGER PRIMARY KEY, "
            + DatabaseContract.Products.NAME + " TEXT, "
            + DatabaseContract.Products.DESCRIPTION + " TEXT, "
            + DatabaseContract.Products.IMAGE_URL + " TEXT, "
            + DatabaseContract.Products.PRICE + " DOUBLE, "
            + "UNIQUE (" + DatabaseContract.Products.ID + ") ON CONFLICT REPLACE)";

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_PRODUCTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + Tables.PRODUCTS);
        onCreate(db);
    }

    public static SQLiteDatabase getWritable(Context context) {
        return DatabaseHelper.getInstance(context).getWritableDatabase();
    }

    public static SQLiteDatabase getReadable(Context context) {
        return DatabaseHelper.getInstance(context).getWritableDatabase();
    }

    private static DatabaseHelper getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new DatabaseHelper(context.getApplicationContext());
        }
        return sInstance;
    }

    public static void closeInstanceEnd() {
        if (sInstance != null) {
            sInstance.close();
        }
    }

    public static void dropDatabase(Context context) {
        boolean delete = context.getApplicationContext().deleteDatabase(DATABASE_NAME);
        sInstance = null;
    }

    public static void deleteDatabase(Context context) {
        context.deleteDatabase(DATABASE_NAME);
    }

    /**
     * Use to export DB on SD-card.
     */
    public static void exportDB(Context context) {
        File sd = Environment.getExternalStorageDirectory();
        File data = Environment.getDataDirectory();
        String currentDBPath = "/data/" + context.getPackageName() + "/databases/" + DATABASE_NAME;
        String backupDBPath = DATABASE_NAME;
        File currentDB = new File(data, currentDBPath);
        File backupDB = new File(sd, backupDBPath);
        try {
            FileChannel source = new FileInputStream(currentDB).getChannel();
            FileChannel destination = new FileOutputStream(backupDB).getChannel();
            destination.transferFrom(source, 0, source.size());
            source.close();
            destination.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
