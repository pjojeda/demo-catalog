package com.example.demo_catalog.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.example.demo_catalog.model.Product;

/**
 * Created by chochito on 18/10/16.
 */

public class ProductDao  extends AbstractSqliteDao<Product>{

    public ProductDao(Context context) {
        super(context);
    }

    @Override
    public String getTableName() {
        return DatabaseHelper.Tables.PRODUCTS;
    }

    @Override
    public String getLocalIdName() {
        return DatabaseContract.Products.ID;
    }

    @Override
    protected ContentValues getContentValues(Product product, boolean isNew) {
        ContentValues contentValues = new ContentValues();
        if(isNew) {
            contentValues.put(DatabaseContract.Products.ID, product.getId());
        }
        contentValues.put(DatabaseContract.Products.DESCRIPTION, product.getDescription());
        contentValues.put(DatabaseContract.Products.IMAGE_URL, product.getUrlImage());
        contentValues.put(DatabaseContract.Products.NAME, product.getName());
        contentValues.put(DatabaseContract.Products.PRICE, product.getPrice());
        return contentValues;
    }

    @Override
    protected Product getEntity(Cursor cursor) {
        Product product = new Product();
        int indexId = cursor.getColumnIndexOrThrow(DatabaseContract.Products.ID);
        long id = cursor.getLong(indexId);
        int indexDescription = cursor.getColumnIndexOrThrow(DatabaseContract.Products.DESCRIPTION);
        String description = cursor.getString(indexDescription);
        int indexImageUrl = cursor.getColumnIndexOrThrow(DatabaseContract.Products.IMAGE_URL);
        String imageUrl = cursor.getString(indexImageUrl);
        int indexName = cursor.getColumnIndexOrThrow(DatabaseContract.Products.NAME);
        String name = cursor.getString(indexName);
        int indexPrice = cursor.getColumnIndexOrThrow(DatabaseContract.Products.PRICE);
        double price = cursor.getDouble(indexPrice);
        product.setId(id);
        product.setDescription(description);
        product.setUrlImage(imageUrl);
        product.setName(name);
        product.setPrice(price);
        return product;
    }
}
