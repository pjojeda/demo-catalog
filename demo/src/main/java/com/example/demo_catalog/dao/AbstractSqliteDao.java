package com.example.demo_catalog.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.demo_catalog.model.BaseEntity;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by chochito on 13/10/15.
 */
public abstract class AbstractSqliteDao<T extends BaseEntity> {

    private Context context;

    private Class<T> persistentClass;

    public AbstractSqliteDao(Context context) {
        this.context = context;

        ParameterizedType thisType = (ParameterizedType) getClass().getGenericSuperclass();
        persistentClass = (Class<T>) thisType.getActualTypeArguments()[0];
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public Class<T> getPersistentClass() {
        return persistentClass;
    }

    public abstract String getTableName();

    public abstract String getLocalIdName();

    public boolean exists(Serializable id) {
        SQLiteDatabase db = DatabaseHelper.getWritable(context);
        Cursor cursor = null;
        if (id != null) {
            try {
                String selection = getSelectionString(id);
                String[] selectionArgs = getSelectionArgs(id);
                if (selection == null) {
                    return false;
                } else {
                    cursor = db.query(getTableName(), null, selection, selectionArgs, null, null, null);
                    boolean exists = cursor.getCount() > 0;
                    return exists;
                }
            }finally {
                if(cursor != null){
                    cursor.close();
                }
            }
        }
        return false;
    }

    public long save(T entity) {
        SQLiteDatabase db = DatabaseHelper.getWritable(context);
        ContentValues values = getContentValues(entity, true);
        return db.insert(getTableName(), null, values);
    }

    public void update(T entity) {
        SQLiteDatabase db = DatabaseHelper.getWritable(context);
        String selection = getSelectionString(entity.getId());
        String[] selectionArgs = getSelectionArgs(entity.getId());
        ContentValues values = getContentValues(entity, false);
        int rows = db.update(getTableName(), values, selection, selectionArgs);
        Log.d("DAO", "updated rows: " + rows);
    }

    public List<T> getAll() {
        SQLiteDatabase db = DatabaseHelper.getWritable(context);
        Cursor cursor = db.query(getTableName(), null, null, null, null, null, null);
        List<T> list = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                T entity = getEntity(cursor);
                list.add(entity);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return list;
    }

    public boolean delete(Serializable id) {
        SQLiteDatabase db = DatabaseHelper.getWritable(context);
        String where = getSelectionString(id);
        String[] selectionArgs = getSelectionArgs(id);
        try {
            int row = db.delete(getTableName(), where, selectionArgs);
            if (row > 0) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public int deleteAll() {
        SQLiteDatabase db = DatabaseHelper.getWritable(context);
        return db.delete(getTableName(), null, null);
    }

    public String getSelectionString(Serializable id) {
        if (id != null) {
            return getLocalIdName() + " = ?";
        } else return null;
    }

    public String[] getSelectionArgs(Serializable id) {
        if (id != null) {
            return new String[]{id.toString()};
        } else return null;
    }

    public T getWithLocalId(Serializable id) {
        T entity = null;
        SQLiteDatabase db = DatabaseHelper.getReadable(context);
        if (id != null) {
                String selection = getSelectionString(id);
                String[] selectionArgs = getSelectionArgs(id);
                if (selection == null) {
                    return null;
                } else {
                    Cursor cursor = db.query(getTableName(), null, selection, selectionArgs, null, null, null);
                    if (cursor != null && cursor.moveToFirst()) {
                        try {
                            entity = getEntity(cursor);
                        } finally {
                            cursor.close();
                        }
                    }
                }
        }
        return entity;
    }

    protected abstract ContentValues getContentValues(T entity, boolean isNew);

    protected abstract T getEntity(Cursor cursor);
}
