package com.example.demo_catalog.manager.listener;

/**
 * Created by chochito on 01/04/16.
 */
public interface OnSingleSearchListener<T> {

    void onSearchCompleted(T entity);

    void onError(String error);
}
