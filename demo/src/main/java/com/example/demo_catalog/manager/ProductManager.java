package com.example.demo_catalog.manager;

import android.content.Context;

import com.example.demo_catalog.dao.ProductDao;
import com.example.demo_catalog.manager.listener.OnSearchListener;
import com.example.demo_catalog.manager.listener.OnSingleSearchListener;
import com.example.demo_catalog.model.Product;
import com.example.demo_catalog.rest.ProductRestClient;
import com.example.demo_catalog.rest.util.RequestManagerEntityListener;
import com.example.demo_catalog.rest.util.RequestManagerException;

import java.util.Collections;
import java.util.List;

/**
 * Created by chochito on 18/10/16.
 */

public class ProductManager {

    private Context context;
    private ProductRestClient productRestClient;
    private ProductDao productDao;

    public ProductManager(Context context){
        this.context = context;
        productRestClient = new ProductRestClient(context);
        productDao = new ProductDao(context);
    }

    public void findAll(int start, final OnSearchListener<Product> listener){
        productRestClient.findAllMockup(new RequestManagerEntityListener<List<Product>>() {
            @Override
            public void onOkResponse(List<Product> list) {
                Collections.sort(list);
                for(Product product: list){
                    if(productDao.exists(product.getId())){
                        productDao.update(product);
                    }else {
                        productDao.save(product);
                    }
                }
                listener.onSearchCompleted(list);
            }

            @Override
            public void onError(RequestManagerException ex) {
                listener.onError(ex.getError());
                List<Product> products = productDao.getAll();
                Collections.sort(products);
                listener.onSearchCompleted(products);
            }
        });
    }

    public void findById(final long id, final OnSingleSearchListener<Product>listener){
        productRestClient.findById(id, new RequestManagerEntityListener<Product>() {
            @Override
            public void onOkResponse(Product entity) {
                productDao.update(entity);
                listener.onSearchCompleted(entity);
            }

            @Override
            public void onError(RequestManagerException ex) {
                Product product = productDao.getWithLocalId(id);
                listener.onSearchCompleted(product);
                listener.onError(ex.getMessage());
            }
        });
    }

}
