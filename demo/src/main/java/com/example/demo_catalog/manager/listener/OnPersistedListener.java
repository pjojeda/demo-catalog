package com.example.demo_catalog.manager.listener;

/**
 * Created by chochito on 22/01/16.
 */
public interface OnPersistedListener<T> {

    void onPersisted(T entity);

    void onError(String message);

}
