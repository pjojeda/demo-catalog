package com.example.demo_catalog.manager.listener;

import java.util.List;

/**
 * Created by chochito on 22/01/16.
 */
public interface OnSearchListener<T> {

    void onSearchCompleted(List<T> list);

    void onError(String error);
}
