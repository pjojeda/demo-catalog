package com.example.demo_catalog.rest.util;

import android.content.Context;
import android.content.DialogInterface;

import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by chochito on 12/10/15.
 */
public class RequestManagerException extends Exception {

    private int statusCode;
    private String message;
    private ErrorRequest errorRequest;
    private Context mContext;

    public static final String MESSAGE_STATUS_503 = "The server is currently unavailable";
    public static final String ERROR_INVALID_CLIENT = "invalid_client";

    public RequestManagerException(Throwable throwable, Context context) {
        super(throwable);
        mContext = context;
        if(throwable instanceof VolleyError){


            VolleyError error = (VolleyError)throwable;
            if(error.networkResponse != null) {
                message = new String(error.networkResponse.data);
                this.statusCode = error.networkResponse.statusCode;
                if(statusCode == 1006){
                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(mContext);
                    builder.setTitle("Version Too Old");
                    builder.setMessage("Please update the app in order to continue.");
                    builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    builder.show();
                }else if(statusCode == 400){

                }else if(statusCode == 401){

                }else if(statusCode == 403){

                }else if(statusCode == 404){

                }else if(statusCode == 500){
                    message = "";
                }else if(statusCode == 503){
                    message = MESSAGE_STATUS_503;
                }
            }else{
                message = error.getMessage();
                if(message == null){
                    message = error.toString();
                }
            }
            try {
                if(message != null) {
                    JSONObject jsonObject = new JSONObject(message);
                    errorRequest = new ErrorRequest();
                    if (jsonObject.has("error")) {
                        errorRequest.setError(jsonObject.getString("error"));
                    } else if (jsonObject.has("code")) {
                        errorRequest.setError(jsonObject.getString("code"));
                    }
                    if (jsonObject.has("message")) {
                        errorRequest.setMessage(jsonObject.getString("message"));
                    } else {
                        errorRequest.setMessage(errorRequest.getError());
                    }

                    if (statusCode == 201) {
                        // This is to fix transitive volley 201 bug
                        errorRequest.setMessage(message);
                    }
                }else{
                    message = "";
                }
            } catch (JSONException e) {
                e.printStackTrace();
                if(message == null) {
                    message = "";
                }
            }
        }
    }

    public int getStatusCode(){
        return this.statusCode;
    }

    @Override
    public String getMessage() {
        return errorRequest != null ? errorRequest.getMessage() : message;
    }

    public String getError(){
        return errorRequest != null ? errorRequest.getError() : message;
    }

    public boolean isTokenExpired(){
        if(errorRequest == null){
            return false;
        }
        if(statusCode == 401 && errorRequest.getMessage().equalsIgnoreCase("Token Expired")||errorRequest.getMessage().equalsIgnoreCase("Token Expired")){
            return true;
        }
        return false;
    }

    private class ErrorRequest{
        private String error = "";
        private String message = "";

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getError() {
            return error;
        }

        public void setError(String error) {
            this.error = error;
        }
    }
}
