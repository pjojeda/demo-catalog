package com.example.demo_catalog.rest.util;

/**
 * Created by chochito on 05/10/15.
 */
public interface RequestManagerListener {

    void onOkResponse();

    void onError(RequestManagerException ex);

}
