package com.example.demo_catalog.rest.util.types;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Base64;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by joaquin on 16/06/15.
 */
public class RequestUtil {

    public static Map<String, String> getHeaders(Context context) {
        Map<String, String> headers = getHeadersGeneral(context);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return headers;
    }

    public static Map<String, String> getHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json; charset=utf-8");
        return headers;
    }

    public static Map<String, String> getHeadersGeneral(Context context) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json; charset=utf-8");
        JSONObject jsonAppInfo = null;
//        try {
//            jsonAppInfo = ApplicationInfo.getAppInfo(context);
//            String version = jsonAppInfo.getString("version");
//            String build = jsonAppInfo.getString("build");
//            headers.put("X-INB-Version", version + " " + build + " Android");
//
//            String appName = jsonAppInfo.getString("appName");
//            String userAgent = System.getProperty("http.agent");
//            String customUserAgent = appName + "/" + version + " " + userAgent.substring(userAgent.indexOf("("));
//            headers.put("User-agent", customUserAgent);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
//        }
        return headers;
    }

    public static String getB64Auth(String username, String pass) {
        String source = username + ":" + pass;
        String ret = "Basic " + Base64.encodeToString(source.getBytes(), Base64.URL_SAFE | Base64.NO_WRAP);
        return ret;
    }

}
