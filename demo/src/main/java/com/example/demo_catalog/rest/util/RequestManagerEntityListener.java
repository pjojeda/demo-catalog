package com.example.demo_catalog.rest.util;

/**
 * Created by chochito on 12/10/15.
 */
public interface RequestManagerEntityListener<T> {

    void onOkResponse(T entity);

    void onError(RequestManagerException ex);

}
