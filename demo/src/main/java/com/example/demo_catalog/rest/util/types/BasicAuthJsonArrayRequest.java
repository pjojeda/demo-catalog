package com.example.demo_catalog.rest.util.types;

import com.android.volley.AuthFailureError;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;

import java.util.Map;


/**
 * Created by joaspacce.
 */
public class BasicAuthJsonArrayRequest extends JsonArrayRequest {

    public BasicAuthJsonArrayRequest(String url, Listener<JSONArray> listener, ErrorListener errorListener) {
        super(url, listener, errorListener);
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return RequestUtil.getHeaders();
    }
}