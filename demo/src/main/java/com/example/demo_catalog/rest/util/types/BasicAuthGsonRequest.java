package com.example.demo_catalog.rest.util.types;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;

import org.json.JSONObject;

import java.util.Map;


/**
 * Created by joaspacce.
 */
public class BasicAuthGsonRequest<T> extends GsonRequest<T> {


    /**
     * Creates a new request with the given method (one of the values from {@link Request.Method}),
     * URL, and error listener.  Note that the normal response listener is not provided here as
     * delivery of responses is provided by subclasses, who have a better idea of how to deliver
     * an already-parsed response.
     *
     * @param method  method request.
     * @param url     URL of the request to make
     * @param clazz   Relevant class object, for Gson's reflection
     * @param headers Map of request headers
     * @param body    Body Paramaters.
     */
    public BasicAuthGsonRequest(int method, String url, Class<T> clazz, Map<String, String> headers,
                                Listener<T> listener, ErrorListener errorListener, JSONObject body) {
        super(method, url, clazz, headers, listener, errorListener, body);
    }

    /**
     * Make a GET request and return a parsed object from JSON.
     *
     * @param url           URL of the request to make
     * @param clazz         Relevant class object, for Gson's reflection
     * @param headers       Map of request headers
     * @param listener
     * @param errorListener
     */
    public BasicAuthGsonRequest(String url, Class<T> clazz, Map<String, String> headers, Listener<T> listener, ErrorListener errorListener) {
        super(url, clazz, headers, listener, errorListener);
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return RequestUtil.getHeaders();
    }
}