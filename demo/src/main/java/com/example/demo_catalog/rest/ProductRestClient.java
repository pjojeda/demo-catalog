package com.example.demo_catalog.rest;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.example.demo_catalog.BuildConfig;
import com.example.demo_catalog.model.Product;
import com.example.demo_catalog.rest.util.RequestManager;
import com.example.demo_catalog.rest.util.RequestManagerEntityListener;
import com.example.demo_catalog.rest.util.RequestManagerException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chochito on 18/10/16.
 */

public class ProductRestClient {

    private Context context;
    private static final int MAX_RESULTS_DEFAULT = 20;
    private static final String URI_PRODUCTOS = "productos";
    private static final String URI_FIND_ALL = URI_PRODUCTOS.concat("/getAll?start=%d&maxResults=%d");
    private static final String URI_FIND_ID = URI_PRODUCTOS.concat("/get/%d");

    public ProductRestClient(Context context){
        this.context = context;
    }

    public void findAllMockup(final RequestManagerEntityListener<List<Product>> listener){
        StringBuilder sb = new StringBuilder(BuildConfig.BASE_URL);
        sb.append("products.json");
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, sb.toString(), new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                List<Product>products = new ArrayList();
                int lenght = response.length();
                for(int index = 0; index < lenght; index++){
                    try {
                        JSONObject json = response.getJSONObject(index);
                        Product product = parseProduct(json);
                        products.add(product);
                    } catch (JSONException e) {
                        RequestManagerException ex = new RequestManagerException(e, context);
                        listener.onError(ex);
                    }
                }
                listener.onOkResponse(products);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                RequestManagerException ex = new RequestManagerException(error, context);
                listener.onError(ex);
            }
        });
        RequestManager.getInstance(context).addToRequestQueue(request);
    }

    public void findAll(final RequestManagerEntityListener<List<Product>> listener, int start){
        StringBuilder sb = new StringBuilder(BuildConfig.BASE_URL);
        sb.append(String.format(URI_FIND_ALL, start, MAX_RESULTS_DEFAULT));
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, sb.toString(), new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                List<Product>products = new ArrayList();
                int lenght = response.length();
                for(int index = 0; index < lenght; index++){
                    try {
                        JSONObject json = response.getJSONObject(index);
                        Product product = parseProduct(json);
                        products.add(product);
                    } catch (JSONException e) {
                        RequestManagerException ex = new RequestManagerException(e, context);
                        listener.onError(ex);
                    }
                }
                listener.onOkResponse(products);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                RequestManagerException ex = new RequestManagerException(error, context);
                listener.onError(ex);
            }
        });
        RequestManager.getInstance(context).addToRequestQueue(request);
    }

    public void findById(long id, final RequestManagerEntityListener<Product> listener){
        StringBuilder sb = new StringBuilder(BuildConfig.BASE_URL);
        sb.append(String.format(URI_FIND_ID, id));
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, sb.toString(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Product product = null;
                try {
                    product = parseProduct(response);
                    listener.onOkResponse(product);
                } catch (JSONException e) {
                    RequestManagerException ex = new RequestManagerException(e, context);
                    listener.onError(ex);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                RequestManagerException ex = new RequestManagerException(error, context);
                listener.onError(ex);
            }
        });
        RequestManager.getInstance(context).addToRequestQueue(request);
    }

    private Product parseProduct(JSONObject jsonObject) throws JSONException {
        long id = jsonObject.getLong("id");
        String name = jsonObject.getString("name");
        String description = jsonObject.getString("description");
        String imageUrl = jsonObject.getString("image_url");
        double price = jsonObject.getDouble("price");
        Product product = new Product();
        product.setId(id);
        product.setName(name);
        product.setDescription(description);
        product.setUrlImage(imageUrl);
        product.setPrice(price);
        return product;
    }

}
