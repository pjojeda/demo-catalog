package com.example.demo_catalog.rest.util;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;
import com.example.demo_catalog.BuildConfig;

/**
 * Created by chochito on 23/09/15.
 */
public class RequestManager {

    public static final String TAG = RequestManager.class.getCanonicalName();

    /**
     * Custom Number of network request dispatcher threads to start.
     */
    private static final int NETWORK_THREAD_POOL_SIZE = 3;

    private static RequestManager mInstance;

    private RequestQueue mRequestQueue;
    private static Context mContext;

    private RequestManager(Context context){
        mContext = context;
        mRequestQueue = getRequestQueue();
    }

    public static synchronized RequestManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new RequestManager(context);
        }
        return mInstance;
    }

    private RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(mContext.getApplicationContext(), new HurlStack());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> request, String tag) {
        addToQueue(request, tag);
    }

    public <T> void addToRequestQueue(Request<T> request) {
        addToQueue(request, "");
    }

    private void addToQueue(Request request, String tag) {
        if (BuildConfig.DEBUG)
            Log.i(TAG, request.getUrl());
        request.setTag(tag);
        RetryPolicy retryPolicy = new DefaultRetryPolicy(
                DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 20,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
        getRequestQueue().add(request);
    }

    public void cancelDownload(String tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }



}
