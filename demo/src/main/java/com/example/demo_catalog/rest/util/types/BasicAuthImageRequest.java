package com.example.demo_catalog.rest.util.types;

import android.graphics.Bitmap;

import com.android.volley.AuthFailureError;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.ImageRequest;

import java.util.Map;

/**
 * Created by joaspacce.
 */
public class BasicAuthImageRequest extends ImageRequest {


    public BasicAuthImageRequest(String url, Listener<Bitmap> listener, int maxWidth, int maxHeight, Bitmap.Config decodeConfig, ErrorListener errorListener) {
        super(url, listener, maxWidth, maxHeight, decodeConfig, errorListener);
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return RequestUtil.getHeaders();
    }
}